/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.19 : Database - pakar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `pakar`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

LOCK TABLES `admin` WRITE;

insert  into `admin`(`username`,`password`) values ('admin','d033e22ae348aeb5660fc2140aec35850c4da997');

UNLOCK TABLES;

/*Table structure for table `gejala` */

DROP TABLE IF EXISTS `gejala`;

CREATE TABLE `gejala` (
  `kode_gejala` varchar(3) NOT NULL,
  `nama_gejala` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_gejala`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gejala` */

LOCK TABLES `gejala` WRITE;

insert  into `gejala`(`kode_gejala`,`nama_gejala`) values ('G01','Demam'),('G02','Sakit Kepala'),('G03','Nyeri saat bicara atau menelan'),('G04','Batuk'),('G05','Hidung Tersumbat'),('G06','Nyeri telinga'),('G07','Nyeri tenggorokan'),('G08','Nyeri leher'),('G09','Pembengkakan kelenjar getah bening'),('G10','Pendarahan hidung'),('G11','Suara serak'),('G12','Leher bengkak'),('G13','Air liur menetes'),('G14','Infeksi sinus'),('G15','Nyeri wajah');

UNLOCK TABLES;

/*Table structure for table `konsultasi` */

DROP TABLE IF EXISTS `konsultasi`;

CREATE TABLE `konsultasi` (
  `id_konsultasi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `username` varchar(20) NOT NULL,
  `kode_penyakit` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_konsultasi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `konsultasi` */

LOCK TABLES `konsultasi` WRITE;

UNLOCK TABLES;

/*Table structure for table `konsultasi_detail` */

DROP TABLE IF EXISTS `konsultasi_detail`;

CREATE TABLE `konsultasi_detail` (
  `id_konsultasi` int(10) unsigned NOT NULL,
  `kode_gejala` varchar(3) NOT NULL,
  `jawaban` enum('Ya','Tidak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `konsultasi_detail` */

LOCK TABLES `konsultasi_detail` WRITE;

UNLOCK TABLES;

/*Table structure for table `penyakit` */

DROP TABLE IF EXISTS `penyakit`;

CREATE TABLE `penyakit` (
  `kode_penyakit` varchar(3) NOT NULL,
  `nama_penyakit` varchar(100) NOT NULL,
  `definisi` text,
  `pengobatan` text,
  `pencegahan` text,
  PRIMARY KEY (`kode_penyakit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `penyakit` */

LOCK TABLES `penyakit` WRITE;

insert  into `penyakit`(`kode_penyakit`,`nama_penyakit`,`definisi`,`pengobatan`,`pencegahan`) values ('P01','Contract Ulcers',NULL,NULL,NULL),('P02','Abses Parafaringeal',NULL,NULL,NULL),('P03','Abses Peritonsiler (Penimbunan Nanah disekitar Amandel)',NULL,NULL,NULL),('P04','Barotitis Media','','',''),('P05','Deviasi Septum (Pergeseran Dinding Hidung)','','',''),('P06','Faringitis (Radang Tenggorokan)','','',''),('P07','Kanker Laring',NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `relasi` */

DROP TABLE IF EXISTS `relasi`;

CREATE TABLE `relasi` (
  `kode_penyakit` varchar(3) NOT NULL,
  `kode_gejala` varchar(3) NOT NULL,
  PRIMARY KEY (`kode_gejala`,`kode_penyakit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `relasi` */

LOCK TABLES `relasi` WRITE;

insert  into `relasi`(`kode_penyakit`,`kode_gejala`) values ('P03','G01'),('P05','G01'),('P06','G01'),('P03','G02'),('P04','G02'),('P01','G03'),('P02','G03'),('P06','G03'),('P07','G03'),('P07','G04'),('P05','G05'),('P04','G06'),('P03','G07'),('P06','G07'),('P07','G07'),('P07','G08'),('P06','G09'),('P05','G10'),('P01','G11'),('P02','G12'),('P03','G13'),('P05','G14'),('P05','G15');

UNLOCK TABLES;

/*Table structure for table `tmp_penyakit` */

DROP TABLE IF EXISTS `tmp_penyakit`;

CREATE TABLE `tmp_penyakit` (
  `username` varchar(20) NOT NULL,
  `kode_penyakit` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tmp_penyakit` */

LOCK TABLES `tmp_penyakit` WRITE;

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `usia` int(10) unsigned DEFAULT NULL,
  `tinggi` int(10) unsigned DEFAULT NULL,
  `berat` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`username`,`password`,`nama`,`jenis_kelamin`,`alamat`,`usia`,`tinggi`,`berat`) values ('admin','',NULL,NULL,NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
