<?php defined('BASEPATH') OR die('No direct access allowed.');

class GejalaModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($param = array()) {
        $sql = 'SELECT * FROM gejala WHERE 1 = 1 ';
        
        $where = array();
        if (isset($param['kode_gejala'])) {
            $sql .= ' AND kode_gejala = ? ';
            array_push($where, $param['kode_gejala']);
        }
        
        if (isset($param['nama_gejala'])) {
            $sql .= ' AND nama_gejala LIKE ? ';
            array_push($where, '%'.$param['nama_gejala'].'%');
        }
        
        return $this->db->query($sql, $where);
    }
    
    public function insert($param = array()) {
        $sql = '
            INSERT INTO gejala (kode_gejala, nama_gejala) 
            VALUES (?, ?) 
        ';
        
        $value = array(strtoupper($param['textKode']), $param['textNama']);
        $this->db->query($sql, $value);
    }
    
    public function update($param = array(), $kode_gejala) {
        $sql = '
            UPDATE gejala SET 
                nama_gejala = ? 
            WHERE kode_gejala = ?
        ';
        
        $value = array($param['textNama'], $kode_gejala);
        $this->db->query($sql, $value);
    }
    
    public function delete($kode_gejala) {
        $sql = '
            DELETE FROM gejala WHERE kode_gejala = ?
        ';
        
        $value = array($kode_gejala);
        $this->db->query($sql, $value);
    }
}
