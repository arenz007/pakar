<?php defined('BASEPATH') OR die('No direct access allowed.');

class RelasiModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($param = array()) {
        $sql = '
            SELECT DISTINCT 
                penyakit.kode_penyakit, nama_penyakit, definisi, pencegahan, pengobatan,
                GROUP_CONCAT(DISTINCT gejala.kode_gejala ORDER BY relasi.kode_gejala SEPARATOR "|") AS kode_gejala,
                GROUP_CONCAT(DISTINCT gejala.nama_gejala ORDER BY relasi.kode_gejala SEPARATOR "|") AS nama_gejala
            FROM relasi
            RIGHT JOIN penyakit ON penyakit.kode_penyakit = relasi.kode_penyakit
            LEFT JOIN gejala ON gejala.kode_gejala = relasi.kode_gejala
            WHERE 1 = 1
        ';
        
        $where = array();
        if (isset($param['kode_penyakit'])) {
            $sql .= ' AND penyakit.kode_penyakit = ? ';
            $where[] = $param['kode_penyakit'];
        }
        
        $sql .= '
            GROUP BY relasi.kode_penyakit 
            ORDER BY penyakit.kode_penyakit 
        ';
        
        return $this->db->query($sql, $where);
    }
    
    public function update($param = array(), $kode_penyakit) {
        $this->db->trans_begin();
        
        $this->db->delete('relasi', array('kode_penyakit' => $kode_penyakit));
        
        $gejala = $param['checkboxGejala'];
        $data = array();
        foreach ($gejala AS $kode_gejala) {
            $data[] = array(
                'kode_penyakit' => $kode_penyakit,
                'kode_gejala'   => $kode_gejala
            );
        }
        
        $this->db->insert_batch('relasi', $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
}