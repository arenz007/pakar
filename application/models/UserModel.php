<?php defined('BASEPATH') OR die('No direct access allowed.');

class UserModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function cekUser($username, $password) {
        $query = $this->db->query('
                SELECT username, `password`, 1 AS `level` FROM admin WHERE username  = ? AND `password` = ?
                UNION ALL
                SELECT username, `password`, 2 AS `level` FROM `user` WHERE username = ? AND `password` = ? 
            ', array($username, $password, $username, $password)
        );
        
        return $query;
    }
	
	public function cekPwd($password) {
		$this->load->library('encrypt');
		$this->load->library('sesi');
		
        $query = $this->db->query('
                SELECT username, `password`, 2 AS `level` FROM `user` WHERE username = ? AND `password` = ? 
            ', array($this->sesiLogin['username'], $this->encrypt->sha1($password))
        );
        
        return $query->num_rows() == 1;
    }
    
    public function get($param = array()) {
        $sql = 'SELECT * FROM `user` WHERE 1 = 1 ';
        
        $where = array();
        if (isset($param['username'])) {
            $sql .= ' AND username = ? ';
            $where[] = $param['username'];
        }
        
        if (isset($param['nama'])) {
            $sql .= ' AND nama LIKE ? ';
            $where[] = '%'.$param['nama'].'%';
        }
        
        return $this->db->query($sql, $where);
    }
    
    public function insert($param = array()) {
        $this->load->library('encrypt');
        
        $sql = '
            INSERT INTO user (username, password, nama, jenis_kelamin, alamat, usia, tinggi, berat) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?) 
        ';
        
        $value = array(
            $param['textUsername'], 
            $this->encrypt->sha1($param['textPassword1']), 
            $param['textNama'], 
            $param['radioJenisKelamin'], 
            $param['textAlamat'],
            $param['textUsia'],
            $param['textTinggi'],
            $param['textBerat']
        );
        
        $this->db->query($sql, $value);
    }
    
    public function update($param = array(), $username) {
        $sql = '
            UPDATE user SET 
                nama = ?, 
                jenis_kelamin = ?, 
                alamat = ?, 
                usia = ?, 
                tinggi = ?, 
                berat = ?
            WHERE username = ? 
        ';
        
        $value = array(
            $param['textNama'], 
            $param['radioJenisKelamin'], 
            $param['textAlamat'],
            $param['textUsia'],
            $param['textTinggi'],
            $param['textBerat'],
            $username
        );
        
        $this->db->query($sql, $value);
    }
	
	public function ubahPwd($password) {
		$this->load->library('encrypt');
		$this->load->library('sesi');
		
        $sql = '
            UPDATE user SET 
                `password` = ?
            WHERE username = ? 
        ';
        
        $value = array(
			$this->encrypt->sha1($password),
			$this->sesiLogin['username']
        );
        
        $this->db->query($sql, $value);
    }
}
