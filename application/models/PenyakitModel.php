<?php defined('BASEPATH') OR die('No direct access allowed.');

class PenyakitModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($param = array()) {
        $sql = 'SELECT * FROM penyakit WHERE 1 = 1 ';
        
        $where = array();
        if (isset($param['kode_penyakit'])) {
            $sql .= ' AND kode_penyakit = ? ';
            array_push($where, $param['kode_penyakit']);
        }
        
        if (isset($param['nama_penyakit'])) {
            $sql .= ' AND nama_penyakit LIKE ? ';
            array_push($where, '%'.$param['nama_penyakit'].'%');
        }
        
        return $this->db->query($sql, $where);
    }
    
    public function insert($param = array()) {
        $sql = '
            INSERT INTO penyakit (kode_penyakit, nama_penyakit, definisi, pengobatan, pencegahan) 
            VALUES (?, ?, ?, ?, ?) 
        ';
        
        $value = array(strtoupper($param['textKode']), $param['textNama'], $param['textDefinisi'], $param['textPengobatan'], $param['textPencegahan']);
        $this->db->query($sql, $value);
    }
    
    public function update($param = array(), $kode_penyakit) {
        $sql = '
            UPDATE penyakit SET 
                nama_penyakit = ?, definisi = ?, pengobatan = ?, pencegahan = ? 
            WHERE kode_penyakit = ?
        ';
        
        $value = array($param['textNama'], $param['textDefinisi'], $param['textPengobatan'], $param['textPencegahan'], $kode_penyakit);
        $this->db->query($sql, $value);
    }
    
    public function delete($kode_penyakit) {
        $sql = '
            DELETE FROM penyakit WHERE kode_penyakit = ?
        ';
        
        $value = array($kode_penyakit);
        $this->db->query($sql, $value);
    }
}
