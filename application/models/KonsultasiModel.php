<?php defined('BASEPATH') OR die('No direct access allowed.');

class KonsultasiModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getGejala($username, $kode_gejala) {
        $kode = implode(',', $kode_gejala);
        $kode = str_replace(",", "','", $kode);
        
        $sql = "
            SELECT relasi.kode_gejala, gejala.nama_gejala
            FROM relasi 
            INNER JOIN gejala ON gejala.kode_gejala = relasi.kode_gejala 
            WHERE kode_penyakit IN (
              SELECT kode_penyakit FROM tmp_penyakit WHERE username = ?
            ) AND relasi.kode_gejala NOT IN('".$kode."')
            GROUP BY relasi.kode_gejala, gejala.nama_gejala
            ORDER BY relasi.kode_gejala ASC
        ";
        
        $value = array($username);
        return $this->db->query($sql, $value);
    }
    
    public function clearTmp($username) {
        $sql = '
            DELETE FROM tmp_penyakit WHERE username = ?
        ';
        
        $value = array($username);
        $this->db->query($sql, $value);
    }
    
    public function insertTmpFirstYa($username, $kode_gejala) {
        $this->db->trans_begin();
        
        $this->clearTmp($username);
        $sql = '
            INSERT INTO tmp_penyakit
            SELECT ?, kode_penyakit FROM penyakit 
            WHERE kode_penyakit IN(
              SELECT kode_penyakit FROM relasi WHERE kode_gejala = ?
            )
        ';
        $value = array($username, $kode_gejala);
        $this->db->query($sql, $value);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
    
    public function insertTmpNextYa($username, $kode_gejala) {
        $this->db->trans_begin();
        
        $this->db->query('DROP TEMPORARY TABLE IF EXISTS tmp');
        $this->db->query('CREATE TEMPORARY TABLE tmp AS SELECT * FROM tmp_penyakit');
        $this->clearTmp($username);
        
        $sql = '
            INSERT INTO tmp_penyakit
            SELECT ?, kode_penyakit FROM tmp 
            WHERE kode_penyakit IN(
              SELECT kode_penyakit FROM relasi WHERE kode_gejala = ?
            )
        ';
        $value = array($username, $kode_gejala);
        $this->db->query($sql, $value);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
    
    public function insertTmpFirstTidak($username, $kode_gejala) {
        $this->db->trans_begin();
        
        $this->clearTmp($username);
        $sql = '
            INSERT INTO tmp_penyakit
            SELECT ?, kode_penyakit FROM penyakit 
            WHERE kode_penyakit NOT IN(
              SELECT kode_penyakit FROM relasi WHERE kode_gejala = ?
              GROUP BY kode_penyakit
            )
        ';
        $value = array($username, $kode_gejala);
        $this->db->query($sql, $value);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
    
    public function insertTmpNextTidak($username, $kode_gejala) {
        $this->db->trans_begin();
        
        $this->db->query('DROP TEMPORARY TABLE IF EXISTS tmp');
        $this->db->query('CREATE TEMPORARY TABLE tmp AS SELECT * FROM tmp_penyakit');
        $this->clearTmp($username);
        
        $sql = '
            INSERT INTO tmp_penyakit
            SELECT ?, kode_penyakit FROM tmp 
            WHERE kode_penyakit NOT IN(
              SELECT kode_penyakit FROM relasi WHERE kode_gejala = ?
              GROUP BY kode_penyakit
            )
        ';
        $value = array($username, $kode_gejala);
        $this->db->query($sql, $value);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
    
    public function getHasil($username) {
        $sql = '
            SELECT * FROM penyakit
            WHERE kode_penyakit = (
                SELECT kode_penyakit FROM tmp_penyakit
                WHERE username = ?
            )
        ';
        
        $value = array($username);
        return $this->db->query($sql, $value);
    }
	
	public function getTmpHasil($username) {
        $sql = '
			SELECT kode_penyakit FROM tmp_penyakit
			WHERE username = ?
        ';
        
        $value = array($username);
        return $this->db->query($sql, $value);
    }
	
	public function getPersen($username, $penyakit) {
        $sql = '
			SELECT kode_penyakit, nama_penyakit, (
				SELECT COUNT(kode_gejala) FROM konsultasi_detail WHERE id_konsultasi = (
					SELECT id_konsultasi FROM konsultasi WHERE username = ? ORDER BY id_konsultasi DESC LIMIT 1
				) AND jawaban = ?
			) / (
				SELECT COUNT(kode_gejala) FROM relasi WHERE kode_penyakit = ?
			) * 100 AS persen
			FROM penyakit 
			WHERE kode_penyakit = ?
        ';
        
        $value = array($username, 'Ya', $penyakit, $penyakit);
		$result = $this->db->query($sql, $value);
        return $result->row();
    }
    
    public function save($username, $konsultasi) {
        $result = $this->getHasil($username);
        $penyakit = NULL;
        if ($result->num_rows() > 0) {
            $data = $result->row();
            $penyakit = $data->kode_penyakit;
        }
            
        $this->db->trans_begin();
        $this->db->insert('konsultasi', 
            array(
                'tanggal'       => date('Y-m-d H:i:s'),
                'username'      => $username,
                'kode_penyakit' => $penyakit,
				'persen' 		=> '100',
            )
        );
        
        $data = array();
        foreach ($konsultasi AS $row) {
            $data[] = array(
                'id_konsultasi' => $this->getId($username),
                'kode_gejala'   => $row['kode_gejala'],
                'jawaban'       => $row['jawaban']
            );
        }
        
        $this->db->insert_batch('konsultasi_detail', $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $this->db->trans_commit();
        }
    }
	
	public function updateSave($username, $penyakit, $persen) {
		$id = $this->getId($username);
		$sql = "
			UPDATE konsultasi SET 
				kode_penyakit = ?,
				persen        = ?
			WHERE id_konsultasi = ?
		";
		
		$value = array($penyakit, $persen, $id);
        $this->db->query($sql, $value);
	}
    
    public function getId($username) {
        $sql = '
            SELECT id_konsultasi FROM konsultasi
            WHERE username = ?
            ORDER BY id_konsultasi DESC LIMIT 1
        ';
        
        $value = array($username);
        $result = $this->db->query($sql, $value);
        $row = $result->row();
        
        return $row->id_konsultasi;
    }
    
    public function get($username) {
        $sql = "
            SELECT id_konsultasi, DATE_FORMAT(tanggal, '%d-%m-%Y') AS tanggal, TIME(tanggal) AS jam, penyakit.nama_penyakit, persen
            FROM konsultasi
            LEFT JOIN penyakit ON penyakit.kode_penyakit = konsultasi.kode_penyakit
            WHERE username = ?
            ORDER BY id_konsultasi DESC
        ";
        
        $value = array($username);
        return $this->db->query($sql, $value);
    }
    
    public function getDetail($idKonsultasi) {
        $histori = $this->db->query("
			SELECT id_konsultasi, DATE_FORMAT(tanggal, '%d-%m-%Y') AS tanggal, TIME(tanggal) AS jam, 
				penyakit.nama_penyakit, definisi, pengobatan, pencegahan, persen
			FROM konsultasi
			LEFT JOIN penyakit ON penyakit.kode_penyakit = konsultasi.kode_penyakit
			WHERE id_konsultasi = ?
		", array($idKonsultasi));
		
		$detail = $this->db->query("
			SELECT konsultasi_detail.kode_gejala,gejala.nama_gejala,jawaban
			FROM konsultasi_detail
			JOIN gejala ON gejala.kode_gejala = konsultasi_detail.kode_gejala
			WHERE id_konsultasi = ?
		", array($idKonsultasi));
		
		return array(
			'histori' => $histori,
			'detail'  => $detail
		);
    }
}