<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('debug_array')) {
    
	function debug_array() {
	   if (func_num_args() === 0)
			return;
		
		$params = func_get_args();
		$output = array();

		foreach ($params as $var) {
			$output[] = '<pre>('.gettype($var).') '.htmlspecialchars(print_r($var, TRUE)).'</pre>';
		}

		return implode("\n", $output);
	}
    
    function key_string($array, $keys) {
        if (empty($array))
			return NULL;
            
		$keys = explode('.', $keys);

		do {
			$key = array_shift($keys);
			if (isset($array[$key])) {
				if (is_array($array[$key]) AND ! empty($keys)) {
					$array = $array[$key];
				}
				else {
					return $array[$key];
				}
			}
			else
				break;
		}
		while (!empty($keys));

		return NULL;
    }
}