<?php defined('BASEPATH') OR die('No direct access allowed.');

class MY_Controller extends CI_Controller {
	
    public $sesiLogin;
    public $layout;
    public $jenisKelamin;
    
	public function __construct() {
		parent::__construct();
        
        $this->sesiLogin = $this->sesi->get('login');
        $this->layout['login'] = $this->sesiLogin;
        
        $this->jenisKelamin = array(
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        );
        
        if (!$this->isValidPage($this->router->fetch_class())) {
            redirect('informasi');
        }
        //$this->output->enable_profiler(TRUE);
	}
    
    public function isLogin() {
        return $this->sesiLogin;
    }
    
    public function isValidPage($currentPage) {
        $publicPage = array('beranda', 'informasi', 'login', 'pendaftaran');
        $adminPage  = array('informasi', 'gejala', 'penyakit', 'relasi', 'logout');
        $userPage   = array('informasi', 'profil', 'konsultasi', 'logout');
        
        $isValidPage = TRUE;
        if ($this->isLogin()) {
            $level = $this->sesiLogin['level'];
            if ($level == 1 AND !in_array($currentPage, $adminPage)) {
                $isValidPage = FALSE;
            }
            
            if ($level == 2 AND !in_array($currentPage, $userPage)) {
                $isValidPage = FALSE;
            }
        }
        else {
            if (!in_array($currentPage, $publicPage)) {
                $isValidPage = FALSE;
            }
        }
        
        return $isValidPage;
    }   
}