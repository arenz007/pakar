<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Profil User</h3>
<div style="text-align: right;"><a href="<?php echo site_url('profil/ubah'); ?>">Ubah Profil</a> | <a href="<?php echo site_url('profil/password'); ?>">Ganti Password</a></div>
<table class="info">
    <tr>
        <td class="info">Nama</td>
        <td class="titik">:</td>
        <td><?php echo $result->nama; ?></td>
    </tr>
    <tr>
        <td class="info">Username</td>
        <td class="titik">:</td>
        <td style="font-style: italic;"><?php echo $result->username; ?></td>
    </tr>
    <tr>
        <td class="info">Alamat</td>
        <td class="titik">:</td>
        <td><?php echo $result->alamat; ?></td>
    </tr>
    <tr>
        <td class="info">Jenis Kelamin</td>
        <td class="titik">:</td>
        <td><?php echo $this->jenisKelamin[$result->jenis_kelamin]; ?></td>
    </tr>
    <tr>
        <td class="info">Usia</td>
        <td class="titik">:</td>
        <td><?php echo $result->usia; ?> tahun</td>
    </tr>
    <tr>
        <td class="info">Tinggi Badan</td>
        <td class="titik">:</td>
        <td><?php echo $result->tinggi; ?> cm</td>
    </tr>
    <tr>
        <td class="info">Berat Badan</td>
        <td class="titik">:</td>
        <td><?php echo $result->berat; ?> kg</td>
    </tr>
</table>
<div class="ruler"></div>
<div style="font-weight: bold; margin-top: 30px;font-size: 14px;">Histori Konsultasi:</div>
<table class="data">
    <tr class="data">
		<th class="data nomor">No</th>
		<th class="data">Tanggal</th>
        <th class="data">Jam</th>
		<th class="data">Hasil Diagnosa</th>
		<th class="data aksi">&nbsp;</th>
    </tr>
	<?php if ($histori->num_rows() > 0) : ?>
		<?php foreach ($histori->result_object() AS $k => $row) : ?>
			<tr class="data">
				<td class="data nomor"><?php echo ($k + 1); ?>.</td>
				<td class="data tanggal"><?php echo $row->tanggal; ?></td>
				<td class="data jam"><?php echo $row->jam; ?></td>
				<td class="data nama"><?php echo empty($row->nama_penyakit) ? 'Anda dnyatakan sehat' : $row->nama_penyakit.' ('.$row->persen.'%)'; ?></td>
				<td class="data aksi">
					<input type="hidden" class="idKonsultasi" value="<?php echo $row->id_konsultasi; ?>" />
					<a href="#" class="detail" title="Lihat detail"><img src="<?php echo base_url("resource/icon/detail.png"); ?>" /></a>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php else : ?>
		<tr class="data">
			<td class="kosong" colspan="5">Tidak ada</td>
		</tr>
	<?php endif; ?>
</table>

<div id="detail" title="Detail Histori Konsultasi" style="display: none;"></div>

<script type="text/javascript">
	
	$(function() {
		$('#detail').dialog({
			autoOpen: false,
			modal: true,
			width: 700,
			height: 400,
			buttons: {
				Selesai: function() {
					$(this).dialog('close');
				}
			}
		});
		
		$('.detail').click(function( event ) {
			$.post(
                '<?php echo site_url('profil/histori'); ?>', 
                {id: $(this).prev().val()}, 
                function(data) {
                    $('#detail').html(data);
					$('#detail').dialog('open');
                }
            );
		});
	});
	
</script>