<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('profil'); ?>">Profil User</a> &raquo; Ubah Profil</h3>
<?php if ($this->sesi->get('msg')) : ?>
    <div class="sukses"><?php echo $this->sesi->get_once('msg'); ?></div>
<?php endif;?>
<form action="" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textUsername">Username</label></td>
            <td class="input">
                <input name="textUsername" id="textUsername" value="<?php echo $result->username; ?>" type="text" style="width: 200px;" disabled="disabled" />
            </td>
        </tr>
    </table>
    <table class="form">
    	<tr>
            <td class="label"><label for="textNama">Nama</label></td>
            <td class="input">
                <input name="textNama" id="textNama" value="<?php echo $result->nama; ?>" type="text" style="width: 100%;" maxlength="100" autofocus="autofocus" />
                <?php echo form_error('textNama'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textAlamat">Alamat</label></td>
            <td class="input">
                <input name="textAlamat" id="textAlamat" value="<?php echo $result->alamat; ?>" type="text" style="width: 100%;" maxlength="200" />
                <?php echo form_error('textAlamat'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label>Jenis Kelamin</label></td>
            <td class="input">
                <input type="radio" name="radioJenisKelamin" id="radioJenisKelaminL" value="L" <?php echo $result->jenis_kelamin == 'L' ? 'checked="checked"' : ''; ?> /> <label for="radioJenisKelaminL" style="margin-right: 20px;">Laki-Laki</label>
                <input type="radio" name="radioJenisKelamin" id="radioJenisKelaminP" value="P" <?php echo $result->jenis_kelamin == 'P' ? 'checked="checked"' : ''; ?> /> <label for="radioJenisKelaminP">Perempuan</label>
                <?php echo form_error('radioJenisKelamin'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textUsia">Usia</label></td>
            <td class="input">
                <input name="textUsia" id="textUsia" value="<?php echo $result->usia; ?>" type="number" style="width: 50px;" max="120" /> tahun
                <?php echo form_error('textUsia'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textTinggi">Tinggi Badan</label></td>
            <td class="input">
                <input name="textTinggi" id="textTinggi" value="<?php echo $result->tinggi; ?>" type="text" style="width: 50px;" maxlength="5" /> cm
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textBerat">Berat Badan</label></td>
            <td class="input">
                <input name="textBerat" id="textBerat" value="<?php echo $result->berat; ?>" type="text" style="width: 50px;" maxlength="5" /> kg
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button simpan" value="Simpan" />
            	<input type="reset" class="button" value="Batal" onclick="window.location.href = '<?php echo site_url('profil'); ?>'" />
    	   </td>
        </tr>
    </table>
</form>