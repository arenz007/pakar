<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('profil'); ?>">Profil User</a> &raquo; Ganti Password</h3>
<?php if ($this->sesi->get('msg')) : ?>
    <div class="sukses"><?php echo $this->sesi->get_once('msg'); ?></div>
<?php endif;?>
<form action="" method="POST">
    <table class="form">
        <tr>
            <td class="label" style="width: 200px;"><label for="textPasswordLama">Password Lama</label></td>
            <td class="input">
                <input name="textPasswordLama" id="textPasswordLama" type="password" style="width: 200px;" autofocus="autofocus" />
				<?php echo form_error('textPasswordLama'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPasswordBaru1">Password Baru</label></td>
            <td class="input">
                <input name="textPasswordBaru1" id="textPasswordBaru1" type="password" style="width: 200px;" />
				<?php echo form_error('textPasswordBaru1'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPasswordBaru2">Ketik Ulang Password Baru</label></td>
            <td class="input">
                <input name="textPasswordBaru2" id="textPasswordBaru2" type="password" style="width: 200px;" />
				<?php echo form_error('textPasswordBaru2'); ?>
            </td>
        </tr>
    	<tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button simpan" value="Simpan" />
    	   </td>
        </tr>
    </table>
</form>