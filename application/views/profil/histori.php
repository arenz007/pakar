<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<div style="text-align: left;">
	<table class="info">
		<tr>
			<td class="info">Tanggal/Jam</td>
			<td class="titik">:</td>
			<td><?php echo $histori->tanggal.' / '.$histori->jam; ?></td>
		</tr>
		<tr>
			<td class="info">Hasil Diagnosa</td>
			<td class="titik">:</td>
			<td style="text-align:justify;">
				<div><?php echo (empty($histori->nama_penyakit)) ? '<em> - Anda dinyatakan sehat - </em>' : '<strong>'.$histori->nama_penyakit.' ('.$histori->persen.'%)'.'</strong>'; ?></div>
				<div><?php echo $histori->definisi; ?></div>
			</td>
		</tr>
		<tr>
			<td class="info">Gejala</td>
			<td class="titik">:</td>
			<td>
                <ul style="margin: 0; padding-left: 20px;">
                <?php
                    foreach ($detail->result() AS $row) {
                        echo '<li>'.$row->nama_gejala.' ('.$row->jawaban.')</li>'; 
                    }
                ?>
                </ul>
            </td>
		</tr>
		<tr>
			<td class="info">Pengobatan</td>
			<td class="titik">:</td>
			<td style="text-align:justify;"><?php echo (empty($histori->pengobatan)) ? '-' : $histori->pengobatan; ?></td>
		</tr>
		<tr>
			<td class="info">Pencegahan</td>
			<td class="titik">:</td>
			<td style="text-align:justify;"><?php echo (empty($histori->pencegahan)) ? '-' : $histori->pencegahan; ?></td>
		</tr>
	</table>
</div>
