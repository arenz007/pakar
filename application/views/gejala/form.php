<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('gejala'); ?>">Gejala</a> &raquo; <?php echo $subtitle; ?></h3>
<form action="<?php echo $action; ?>" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textKode">Kode Gejala</label></td>
            <td class="input">
                <input name="textKode" id="textKode" type="text" style="width: 150px;text-transform: uppercase;" maxlength="3" autofocus="autofocus" value="<?php echo (isset($result)) ? $result->kode_gejala : set_value('textKode'); ?>" <?php echo (isset($result)) ? 'readonly="readonly"' : ''; ?> />
                <?php echo form_error('textKode'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textNama">Nama Gejala</label></td>
            <td class="input">
                <input name="textNama" id="textNama" type="text" style="width: 100%;" maxlength="100" value="<?php echo (isset($result)) ? $result->nama_gejala : set_value('textNama'); ?>" />
                <?php echo form_error('textNama'); ?>
            </td>
        </tr>
    	<tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button simpan" value="Simpan" />
            	<input type="reset" class="button" value="Batal" onclick="window.location.href = '<?php echo site_url('gejala'); ?>'" />
    	   </td>
        </tr>
    </table>
</form>