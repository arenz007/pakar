<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Login</h3>
<?php if ($this->sesi->get('gagal')) : ?>
    <div class="gagal"><?php echo $this->sesi->get_once('gagal'); ?></div>
<?php endif;?>
<form action="" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textUsername">Username</label></td>
            <td class="input">
                <input name="textUsername" id="textUsername" type="text" style="width: 200px;" autofocus="autofocus" />
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPassword">Password</label></td>
            <td class="input">
                <input name="textPassword" id="textPassword" type="password" style="width: 200px;" />
            </td>
        </tr>
    	<tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button masuk" value="Login" />
    	   </td>
        </tr>
    </table>
</form>