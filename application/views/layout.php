<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Sistem Pakar Pendiagnosis Penyakit THT</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo base_url("resource/icon/icon.png"); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("resource/js/jqueryui/jquery-ui.min.css"); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("resource/css/layout.css"); ?>" />
        <script type="text/javascript" charset="utf-8" src="<?php echo base_url("resource/js/jquery-1.11.1.min.js"); ?>"></script>
		<script type="text/javascript" charset="utf-8" src="<?php echo base_url("resource/js/jqueryui/jquery-ui.min.js"); ?>"></script>
	</head>
	<body>
        <div id="header">
            <div class="inHeader">
                <div class="judul">SISTEM PAKAR PENDIAGNOSIS PENYAKIT THT</div>
                <div class="mosAdmin">
                    <?php if (!$this->sesiLogin) : ?>
                        <a href="<?php echo site_url('login'); ?>">Login</a> | <a href="<?php echo site_url('pendaftaran'); ?>">Daftar</a>
                    <?php else : ?>
                        Hallo, <a href="<?php echo site_url('profil'); ?>"><?php echo $this->sesiLogin['username']; ?></a>
                    <?php endif; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="wrapper">
            <div id="leftBar">
                <ul>
					<?php if (!$this->sesiLogin) : ?>
						<li><a href="<?php echo site_url('beranda'); ?>">Beranda</a></li>
					<?php endif; ?>
                    <li><a href="<?php echo site_url('informasi'); ?>">Informasi</a></li>
                    <?php if (!$this->sesiLogin) : ?>
                        <li><a href="<?php echo site_url('login'); ?>">Login</a></li>
                        <li><a href="<?php echo site_url('pendaftaran'); ?>">Pendaftaran</a></li>
                    <?php else : ?>
                        <?php if ($this->sesiLogin['level'] == 1) : ?>
                            <li><a href="<?php echo site_url('penyakit'); ?>">Daftar Penyakit</a></li>
                            <li><a href="<?php echo site_url('gejala'); ?>">Daftar Gejala</a></li>
                            <li><a href="<?php echo site_url('relasi'); ?>">Gejala Penyakit</a></li>
                        <?php else : ?>
                            <li><a href="<?php echo site_url('profil'); ?>">Profil</a></li>
                            <li><a href="<?php echo site_url('konsultasi'); ?>">Konsultasi</a></li>
                        <?php endif; ?>
                        <li class="logout"><a href="<?php echo site_url('logout'); ?>">Logout</a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div id="rightContent">
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>
            <div id="footer">&copy; 2016 Universitas Diponegoro</div>
        </div>
    </body>
</html>