<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Pendaftaran</h3>
<?php if ($this->sesi->get('msg')) : ?>
    <div class="sukses"><?php echo $this->sesi->get_once('msg'); ?></div>
<?php endif;?>
<form action="" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textUsername">Username</label></td>
            <td class="input">
                <input name="textUsername" id="textUsername" value="<?php echo set_value('textUsername'); ?>" type="text" style="width: 200px;" autofocus="autofocus" maxlength="20" />
                <?php echo form_error('textUsername'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPassword1">Password</label></td>
            <td class="input">
                <input name="textPassword1" id="textPassword1" type="password" style="width: 200px;" maxlength="10" />
                <?php echo form_error('textPassword1'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPassword2">Ketik Ulang Password</label></td>
            <td class="input">
                <input name="textPassword2" id="textPassword2" type="password" style="width: 200px;" maxlength="10" />
                <?php echo form_error('textPassword2'); ?>
            </td>
        </tr>
    </table>
    <div class="ruler"></div>
    <table class="form">
    	<tr>
            <td class="label"><label for="textNama">Nama</label></td>
            <td class="input">
                <input name="textNama" id="textNama" value="<?php echo set_value('textNama'); ?>" type="text" style="width: 100%;" maxlength="100" />
                <?php echo form_error('textNama'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textAlamat">Alamat</label></td>
            <td class="input">
                <input name="textAlamat" id="textAlamat" value="<?php echo set_value('textAlamat'); ?>" type="text" style="width: 100%;" maxlength="200" />
                <?php echo form_error('textAlamat'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label>Jenis Kelamin</label></td>
            <td class="input">
                <input type="radio" name="radioJenisKelamin" id="radioJenisKelaminL" value="L" <?php echo set_radio('radioJenisKelamin', 'L'); ?> /> <label for="radioJenisKelaminL" style="margin-right: 20px;">Laki-Laki</label>
                <input type="radio" name="radioJenisKelamin" id="radioJenisKelaminP" value="P" <?php echo set_radio('radioJenisKelamin', 'P'); ?> /> <label for="radioJenisKelaminP">Perempuan</label>
                <?php echo form_error('radioJenisKelamin'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textUsia">Usia</label></td>
            <td class="input">
                <input name="textUsia" id="textUsia" value="<?php echo set_value('textUsia'); ?>" type="number" style="width: 50px;" max="120" /> tahun
                <?php echo form_error('textUsia'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textTinggi">Tinggi Badan</label></td>
            <td class="input">
                <input name="textTinggi" id="textTinggi" value="<?php echo $this->input->post('textTinggi'); ?>" type="text" style="width: 50px;" maxlength="5" /> cm
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textBerat">Berat Badan</label></td>
            <td class="input">
                <input name="textBerat" id="textBerat" value="<?php echo $this->input->post('textBerat'); ?>" type="text" style="width: 50px;" maxlength="5" /> kg
            </td>
        </tr>
    </table>
    <div class="ruler"></div>
    <table class="form">
        <tr>
            <td class="label">&nbsp;</td>
            <td>
            	<input type="submit" class="button daftar" value="Daftar" />
    	   </td>
        </tr>
    </table>
</form>