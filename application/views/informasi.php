<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Informasi Daftar Penyakit</h3>
<?php foreach ($result->result_object() AS $k => $row) : ?>
    <table class="info">
        <tr>
            <td class="no" rowspan="6"><?php echo ($k+1); ?>.</td>
            <td class="info">Kode Penyakit</td>
            <td class="titik">:</td>
            <td><?php echo $row->kode_penyakit; ?></td>
        </tr>
        <tr>
            <td class="info">Nama Penyakit</td>
            <td class="titik">:</td>
            <td><?php echo $row->nama_penyakit; ?></td>
        </tr>
        <tr>
            <td class="info">Definisi</td>
            <td class="titik">:</td>
            <td style="text-align:justify;"><?php echo (empty($row->definisi)) ? '-' : $row->definisi; ?></td>
        </tr>
        <tr>
            <td class="info">Pencegahan</td>
            <td class="titik">:</td>
            <td style="text-align:justify;"><?php echo (empty($row->pencegahan)) ? '-' : $row->pencegahan; ?></td>
        </tr>
        <tr>
            <td class="info">Pengobatan</td>
            <td class="titik">:</td>
            <td style="text-align:justify;"><?php echo (empty($row->pengobatan)) ? '-' : $row->pengobatan; ?></td>
        </tr>
        <tr>
            <td class="info">Gejala yang ditimbulkan</td>
            <td class="titik">:</td>
            <td>
                <ul style="margin: 0; padding-left: 20px;">
                <?php
                    $gejalaArray = explode('|', $row->nama_gejala);
                    foreach ($gejalaArray AS $gejala) {
                        echo '<li>'.$gejala.'</li>'; 
                    }
                ?>
                </ul>
            </td>
        </tr>
    </table>
    <div class="ruler"></div>
<?php endforeach; ?>