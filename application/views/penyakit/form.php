<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('penyakit'); ?>">Penyakit</a> &raquo; <?php echo $subtitle; ?></h3>
<form action="<?php echo $action; ?>" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textKode">Kode Penyakit</label></td>
            <td class="input">
                <input name="textKode" id="textKode" type="text" style="width: 150px;text-transform: uppercase;" maxlength="3" autofocus="autofocus" value="<?php echo (isset($result)) ? $result->kode_penyakit : set_value('textKode'); ?>" <?php echo (isset($result)) ? 'readonly="readonly"' : ''; ?> />
                <?php echo form_error('textKode'); ?>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textNama">Nama Penyakit</label></td>
            <td class="input">
                <input name="textNama" id="textNama" type="text" style="width: 100%;" maxlength="100" value="<?php echo (isset($result)) ? $result->nama_penyakit : set_value('textNama'); ?>" />
                <?php echo form_error('textNama'); ?>
            </td>
        </tr>
    	<tr>
            <td class="label"><label for="textDefinisi">Definisi</label></td>
            <td class="input">
                <textarea name="textDefinisi" id="textDefinisi" style="width: 100%; height: 100px;"><?php echo (isset($result)) ? $result->definisi : set_value('textDefinisi'); ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPengobatan">Pengobatan</label></td>
            <td class="input">
                <textarea name="textPengobatan" id="textPengobatan" style="width: 100%; height: 100px;"><?php echo (isset($result)) ? $result->pengobatan : set_value('textPengobatan'); ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textPencegahan">Pencegahan</label></td>
            <td class="input">
                <textarea name="textPencegahan" id="textPencegahan" style="width: 100%; height: 100px;"><?php echo (isset($result)) ? $result->pencegahan : set_value('textPencegahan'); ?></textarea>
            </td>
        </tr>
    	<tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button simpan" value="Simpan" />
            	<input type="reset" class="button" value="Batal" onclick="window.location.href = '<?php echo site_url('penyakit'); ?>'" />
    	   </td>
        </tr>
    </table>
</form>