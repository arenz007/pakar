<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('penyakit'); ?>">Penyakit</a> &raquo; <?php echo $subtitle; ?></h3>
<div class="toolbar">
    <a href="<?php echo site_url('penyakit'); ?>" class="kembali">Kembali</a>
</div>
<table class="info">
    <tr>
        <td class="info">Kode Penyakit</td>
        <td class="titik">:</td>
        <td><?php echo $result->kode_penyakit; ?></td>
    </tr>
    <tr>
        <td class="info">Nama Penyakit</td>
        <td class="titik">:</td>
        <td><?php echo $result->nama_penyakit; ?></td>
    </tr>
    <tr>
		<td class="info">Definisi</td>
		<td class="titik">:</td>
		<td style="text-align:justify;"><?php echo (empty($result->definisi)) ? '-' : $result->definisi; ?></td>
	</tr>
	<tr>
		<td class="info">Pencegahan</td>
		<td class="titik">:</td>
		<td style="text-align:justify;"><?php echo (empty($result->pencegahan)) ? '-' : $result->pencegahan; ?></td>
	</tr>
	<tr>
		<td class="info">Pengobatan</td>
		<td class="titik">:</td>
		<td style="text-align:justify;"><?php echo (empty($result->pengobatan)) ? '-' : $result->pengobatan; ?></td>
	</tr>
	<tr>
		<td class="info">Gejala yang ditimbulkan</td>
		<td class="titik">:</td>
		<td>
			<ul style="margin: 0; padding-left: 20px;">
			<?php
				$gejalaArray = explode('|', $result->nama_gejala);
				foreach ($gejalaArray AS $gejala) {
					echo '<li>'.$gejala.'</li>'; 
				}
			?>
			</ul>
		</td>
	</tr>
</table>