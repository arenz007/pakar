<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Penyakit</h3>
<div class="toolbar">
    <a href="<?php echo site_url('penyakit/tambah'); ?>" class="tambah">Tambah</a>
</div>
<table class="data">
    <tr class="data">
    	<th class="data nomor">No</th>
    	<th class="data">Kode</th>
    	<th class="data">Nama Penyakit</th>
    	<th class="data aksi">Pilihan</th>
    </tr>
    <?php foreach ($result->result_object() AS $k => $row) : ?>
        <tr class="data">
        	<td class="data nomor"><?php echo ($k+1); ?>.</td>
        	<td class="data kode"><?php echo $row->kode_penyakit; ?></td>
        	<td class="data nama"><?php echo $row->nama_penyakit; ?></td>
        	<td class="data aksi">
                <a href="#" class="detail" title="Lihat detail"><img src="<?php echo base_url("resource/icon/detail.png"); ?>" /></a>
                <a href="#" class="edit" title="Edit"><img src="<?php echo base_url("resource/icon/edit.png"); ?>" /></a>
                <a href="#" class="hapus" title="Hapus"><img src="<?php echo base_url("resource/icon/hapus.png"); ?>" /></a>
        	</td>
        </tr>
    <?php endforeach; ?>
</table>

<script type="text/javascript" charset="utf-8">

    $(function() {
        $('.edit').click(function() {
            $.post(
                '<?php echo site_url('penyakit/edit'); ?>', 
                {kode: $(this).parent().parent().children('td.kode').html()}, 
                function(data) {
                    window.location.href = '<?php echo site_url('penyakit/edit'); ?>';
                }
            );
        });
        
        $('.hapus').click(function() {
            var kode = $(this).parent().parent().children('td.kode').html();
            var nama = $(this).parent().parent().children('td.nama').html();
            if (!confirm("Hapus data ini?\n"+kode+" - "+nama)) {
                return;
            }
            
            $.post(
                '<?php echo site_url('penyakit/hapus'); ?>', 
                {kode: $(this).parent().parent().children('td.kode').html()}, 
                function(data) {
                    window.location.href = '<?php echo site_url('penyakit'); ?>';
                }
            );
        });
        
        $('.detail').click(function() {
			 $.post(
                '<?php echo site_url('penyakit/detail'); ?>', 
                {kode: $(this).parent().parent().children('td.kode').html()}, 
                function(data) {
                    window.location.href = '<?php echo site_url('penyakit/detail'); ?>';
                }
            );
        });
        
        <?php if ($this->sesi->get('alert')) : ?>
            alert('<?php echo $this->sesi->get_once('alert'); ?>');
        <?php endif;?>
    });
    
</script>