<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('relasi'); ?>">Gejala Penyakit</a> &raquo; Edit Data</h3>
<form action="<?php echo site_url('relasi/edit'); ?>" method="POST">
    <table class="form">
    	<tr>
            <td class="label"><label for="textKode">Kode Penyakit</label></td>
            <td class="input">
                <input name="textKode" id="textKode" type="text" value="<?php echo $result->kode_penyakit; ?>" style="width: 150px;text-transform: uppercase;" maxlength="3" autofocus="autofocus" disabled="disabled" />
            </td>
        </tr>
        <tr>
            <td class="label"><label for="textNama">Nama Penyakit</label></td>
            <td class="input">
                <input name="textNama" id="textNama" type="text" value="<?php echo $result->nama_penyakit; ?>" style="width: 100%;" maxlength="100" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td class="label">Gejala</td>
            <td class="input">
                <?php foreach ($gejala->result_object() AS $row) : ?>
                    <?php 
                        $kodeGejalaArray = explode('|', $result->kode_gejala);
                        $checked = in_array($row->kode_gejala, $kodeGejalaArray) ? 'checked="checked"' : '';
                    ?>
                    <div style="margin-top: 5px;">
                        <input type="checkbox" name="checkboxGejala[]" id="checkbox<?php echo $row->kode_gejala; ?>" value="<?php echo $row->kode_gejala; ?>" <?php echo $checked; ?> />
                        <label for="checkbox<?php echo $row->kode_gejala; ?>"><?php echo $row->nama_gejala; ?></label>
                    </div>
                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
            	<input type="submit" class="button simpan" value="Simpan" />
            	<input type="reset" class="button" value="Batal" onclick="window.location.href = '<?php echo site_url('relasi'); ?>'" />
    	   </td>
        </tr>
     </table>
</form>