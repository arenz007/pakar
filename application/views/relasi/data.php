<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Gejala Penyakit</h3>
<table class="data" style="margin-top: 15px;">
    <tr class="data">
    	<th class="data nomor">No</th>
    	<th class="data">Kode</th>
    	<th class="data">Penyakit</th>
        <th class="data">Gejala</th>
    	<th class="data aksi">Pilihan</th>
    </tr>
    <?php foreach ($result->result_object() AS $k => $row) : ?>
        <tr class="data">
        	<td class="data nomor"><?php echo ($k+1); ?>.</td>
        	<td class="data kode"><?php echo $row->kode_penyakit; ?></td>
        	<td class="data nama"><?php echo $row->nama_penyakit; ?></td>
            <td class="data">
                <ul style="margin: 0; padding-left: 20px;">
                <?php
					if (strpos($row->nama_gejala, '|')) {
						$gejalaArray = explode('|', $row->nama_gejala);
						foreach ($gejalaArray AS $gejala) {
							echo '<li>'.$gejala.'</li>'; 
						}
					}
                ?>
                </ul>
            </td>
        	<td class="data aksi">
                <a href="#" class="edit" title="Edit"><img src="<?php echo base_url("resource/icon/edit.png"); ?>" /></a>
        	</td>
        </tr>
    <?php endforeach; ?>
</table>

<script type="text/javascript" charset="utf-8">

    $(document).ready(function() {
        $('.edit').click(function() {
            $.post(
                '<?php echo site_url('relasi/edit'); ?>', 
                {kode: $(this).parent().parent().children('td.kode').html()}, 
                function(data) {
                    window.location.href = '<?php echo site_url('relasi/edit'); ?>';
                }
            );
        });
        
        <?php if ($this->sesi->get('alert')) : ?>
            alert('<?php echo $this->sesi->get_once('alert'); ?>');
        <?php endif;?>
    });
    
</script>