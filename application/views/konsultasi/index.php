<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Konsultasi</h3>

<div style="margin: 20px;">
    <div style="font-size: 13px;">Apakah anda mengalami gejala berikut ini?</div>
    <div style="font-size: 14px;font-weight: bold;margin: 10px;">
        &raquo; <span id="namaGejala"><?php echo $result->nama_gejala; ?></span>
        <span id="kodeGejala" style="visibility: hidden;"><?php echo $result->kode_gejala; ?></span>
    </div>
    <div style=" margin: 30px;text-align: center;">
        <input type="submit" class="button ya" value="Ya" />
        <input type="submit" class="button tidak" value="Tidak" />
        <input type="submit" class="button batal" value="Ulangi" />
    </div>
</div> 

<script type="text/javascript" charset="utf-8">

    function prosesJawaban(jawaban) {
        $.ajax({
            url: '<?php echo site_url('konsultasi/jawab'); ?>',
            type: 'POST',
            data: {kodeGejala: $('#kodeGejala').html(), jawab: jawaban},
            dataType: "json",
            success: function(data) {
                if (data.kode) {
                    $('#kodeGejala').html(data.kode);
                    $('#namaGejala').html(data.nama);
                }
                
                if (data.selesai) {
                    window.location.href = '<?php echo site_url('konsultasi/hasil'); ?>';
                }
            }
        });
    }

    $(document).ready(function() {
        
        $('.ya').click(function() {
            prosesJawaban('ya');
        });
        
        $('.tidak').click(function() {
            prosesJawaban('tidak');
        });
        
        $('.batal').click(function() {
            window.location.href = '<?php echo site_url('konsultasi'); ?>';
        });
        
    });

</script>