<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3><a href="<?php echo site_url('konsultasi'); ?>">Konsultasi</a> &raquo; Hasil</h3>

<div style="margin: 20px;">
        <?php if (isset($penyakit)) : ?>
            <div style="font-size: 13px;">Pasien Terdiagnosa Penyakit:</div>
            <div style="font-size: 14px;font-weight: bold;margin: 0px 0 15px 30px;"><?php echo $penyakit->nama_penyakit; ?></div>
            <div style="font-size: 13px;">Gejala yang ditimbulkan:</div>
            <div style="font-size: 14px;font-weight: bold;margin: 0px 0 15px 30px;"><?php echo str_replace('|', ', ', $gejala->nama_gejala); ?></div>
        <?php elseif (isset($persen)) : ?>
            <div style="font-size: 13px;">Pasien Terdiagnosa Penyakit:</div>
            <div style="font-size: 14px;font-weight: bold;margin: 0px 0 15px 30px;"><?php echo $persen->nama_penyakit; ?> dengan tingkat hipotesis <?php echo round($persen->persen); ?>%</div>
        <?php else : ?>
			<div style="font-size: 14px;font-weight: bold;text-align:center;">Anda dinyatakan sehat</div>
		<?php endif; ?>
	<div style="text-align: center;"><a href="<?php echo site_url('profil/histori'); ?>">Lihat selengkapnya</a>
    <div style=" margin: 30px;text-align: center;">
        <input type="submit" class="button batal" value="Ulangi" />
    </div>
</div>

<script type="text/javascript" charset="utf-8">

    $(document).ready(function() {
        
        $('.batal').click(function() {
            window.location.href = '<?php echo site_url('konsultasi'); ?>';
        });
        
    });

</script>
