<?php defined('BASEPATH') OR die('No direct access allowed.'); ?>

<h3>Selamat Datang</h3>

<img src="resource/image/coversispak.png" width="700" height="400"> 
	
	<div>
		<p style="text-align:justify;">Penyakit THT (telinga, hidung, tenggorok) merupakan salah satu jenis penyakit yang cukup sering ditemukan pada masyarakat. Cabang ilmu yang kedokteran yang khusus meneliti diagnosis penyakit telinga, hidung, dan tenggorok serta kepala dan leher disebut dengan Otolaringologi. Pemeriksaan telinga, hidung, dan tenggorok (THT) harus menjadi satu kesatuan karena ketiganya saling berhubungan. Bila ada satu bagian dari organ tersebut terganggu, maka kedua organ lainnya akan ikut terganggu. Untuk itu diperlukan diagnosis oleh dokter ahli untuk mengetahui jenis penyakit yang diderita oleh pasien</p>

		<p></p>		
		
		<p>Sistem pakar pendiagnosis dini penyakit THT ini berfungsi mempercepat pasien dalam hal konsultasi tanpa terbatas waktu dan tempat, serta memberikan saran untuk tindakan penanganan lebih dini terhadap penyakit THT yang diderita.</p>

	</div>
