<?php defined('BASEPATH') OR die('No direct access allowed.');

class Sesi {
    
    protected $CI;
    
    public function __construct() {
        $this->CI =& get_instance();
        
        session_name($this->CI->config->item('sess_cookie_name'));
        session_start();
    }
    
    public function id() {
        return $_SESSION['session_id'];
    }
    
    public function get($key = FALSE, $default = FALSE) {
        if (empty($key))
			return $_SESSION;

        $this->CI->load->helper('text');
		$result = isset($_SESSION[$key]) ? $_SESSION[$key] : key_string($_SESSION, $key);

		return ($result === NULL) ? $default : $result;
    }
    
    public function set($keys, $val = FALSE) {
        if (empty($keys))
			return FALSE;
            
        if (FALSE === is_array($keys))
			$keys = array($keys => $val);
            
        foreach ($keys as $key => $val) {
			$_SESSION[$key] = $val;
		}
    }
    
    public function get_once($key, $default = FALSE) {
        $return = $this->get($key, $default);
        $this->delete($key);
    
        return $return;
    }
    
    public function delete($keys) {
        $args = func_get_args();

		foreach ($args as $key) {
			unset($_SESSION[$key]);
		}
    }
}