<?php defined('BASEPATH') OR die('No direct access allowed.');

class Informasi extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('RelasiModel', 'relasi');
    }
    
	public function index() {
        $result = $this->relasi->get();
        $data['result'] = $result;
        
        $this->layout['content'] = $this->load->view('informasi', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
}