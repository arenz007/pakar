<?php defined('BASEPATH') OR die('No direct access allowed.');

class Login extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
	public function index() {
        if ($_POST) {
            $this->load->library('encrypt');
            $this->load->model('UserModel', 'user');
            $result = $this->user->cekUser($this->input->post('textUsername'), $this->encrypt->sha1($this->input->post('textPassword')));
            if ($result->num_rows() == 1) {
                $data = $result->row();
                $this->sesi->set(array(
                    'login' => array(
                        'username' => $data->username,
                        'level'    => $data->level,
                    )
                ));
                
                redirect('informasi');
            }
            else {
                $this->sesi->set('gagal', 'Login gagal, silahkan ulangi.');
                redirect('login');
            }
        }
        
		$this->layout['content'] = $this->load->view('login', '', TRUE);
        $this->load->view('layout', $this->layout);
	}
}
