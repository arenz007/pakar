<?php defined('BASEPATH') OR die('No direct access allowed.');

class Beranda extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
	public function index() {
        $this->layout['content'] = $this->load->view('beranda', '', TRUE);
        $this->load->view('layout', $this->layout);
	}
}