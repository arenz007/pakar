<?php defined('BASEPATH') OR die('No direct access allowed.');

class Relasi extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('RelasiModel', 'relasi');
        $this->load->model('GejalaModel', 'gejala');
    }
    
	public function index() {
        $this->sesi->delete('edit');
        $result = $this->relasi->get();
        $data['result'] = $result;
        
        $this->layout['content'] = $this->load->view('relasi/data', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
    
    public function edit() {
        if ($this->input->is_ajax_request()) {
            $this->sesi->set(array('edit' => array('kode' => $this->input->post('kode'))));
            exit;
        }
        
        if ($this->sesi->get('edit')) {
            $edit   = $this->sesi->get('edit');
            $result = $this->relasi->get(array('kode_penyakit' => $edit['kode']));
            $gejala = $this->gejala->get();
            $data['result'] = $result->row();
            $data['gejala'] = $gejala;
        }
        else {
            redirect('relasi');
        }
        
        if ($_POST) {
            $this->relasi->update($this->input->post(), $edit['kode']);
            $this->sesi->set('alert', 'Data telah diedit');
            redirect('relasi');
        }
        
        $this->layout['content'] = $this->load->view('relasi/form', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
}