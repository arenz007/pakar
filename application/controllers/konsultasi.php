<?php defined('BASEPATH') OR die('No direct access allowed.');

class Konsultasi extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('GejalaModel', 'gejala');
        $this->load->model('RelasiModel', 'relasi');
        $this->load->model('KonsultasiModel', 'konsultasi');
    }
    
	public function index() {
        $this->sesi->set('first', '1');
        $this->sesi->delete('konsultasi');
		$this->sesi->delete('penyakit');
        
        $result = $this->gejala->get();
        $data['result'] = $result->row();
        $this->konsultasi->clearTmp($this->sesiLogin['username']);
        
        $this->layout['content'] = $this->load->view('konsultasi/index', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
    
    public function jawab() {
        if ($_POST AND $this->input->is_ajax_request()) {
            $konsultasi = $this->sesi->get('konsultasi');
            $konsultasi[] = array(
                'kode_gejala' => $this->input->post('kodeGejala'),
                'jawaban'     => $this->input->post('jawab'),
            );
            
            $this->sesi->set('konsultasi', $konsultasi);
            
            $first   = $this->sesi->get('first') == '1';
            $jawabYa = $this->input->post('jawab') == 'ya';
            
            if ($first) {
                if ($jawabYa) {
                    $this->konsultasi->insertTmpFirstYa($this->sesiLogin['username'], $this->input->post('kodeGejala'));
                }
                else {
                    $this->konsultasi->insertTmpFirstTidak($this->sesiLogin['username'], $this->input->post('kodeGejala'));
                }
            }
            else {
                if ($jawabYa) {
                    $this->konsultasi->insertTmpNextYa($this->sesiLogin['username'], $this->input->post('kodeGejala'));
                }
                else {
                    $this->konsultasi->insertTmpNextTidak($this->sesiLogin['username'], $this->input->post('kodeGejala'));
                }
            }
            
            $this->sesi->set('first', '0');
			
			$penyakit = $this->konsultasi->getTmpHasil($this->sesiLogin['username']);
			if ($penyakit->num_rows() == 1) {
				$data = $penyakit->row();
				$this->sesi->set('penyakit', $data->kode_penyakit);
			}
            
            $kode_gejala = array();
            foreach ($konsultasi AS $row) {
                $kode_gejala[] = $row['kode_gejala'];
            }
            
            $result = $this->konsultasi->getGejala($this->sesiLogin['username'], $kode_gejala);
            
            if ($result->num_rows() > 0) {
                $data = $result->row();
                echo json_encode(
                    array(
                        'kode' => $data->kode_gejala,
                        'nama' => $data->nama_gejala
                    )
                );
            }
            else {
                $this->konsultasi->save($this->sesiLogin['username'], $this->sesi->get('konsultasi'));
                echo json_encode(
                    array(
                        'selesai' 	=> true
                    )
                );
            }
        }
    }
    
    public function hasil() {
        $penyakit = $this->konsultasi->getHasil($this->sesiLogin['username']);
        
        $data = '';
        if ($penyakit->num_rows() > 0) {
            $data['penyakit'] = $penyakit->row();
            $kodePenyakit = $data['penyakit']->kode_penyakit;
            
            $gejala = $this->relasi->get(array('kode_penyakit' => $kodePenyakit));
            $data['gejala'] = $gejala->row();
        }
		else {
			if ($this->sesi->get('penyakit')) {
				$persen   		= $this->konsultasi->getPersen($this->sesiLogin['username'], $this->sesi->get('penyakit'));
				$data['persen'] = $persen;
				
				$this->konsultasi->updateSave($this->sesiLogin['username'], $this->sesi->get('penyakit'), $persen->persen);
			}
		}
        
        $this->layout['content'] = $this->load->view('konsultasi/hasil', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
}