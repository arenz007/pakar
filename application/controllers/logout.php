<?php defined('BASEPATH') OR die('No direct access allowed.');

class Logout extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
	public function index() {
        $this->sesi->delete('login');
        redirect('');
	}
}