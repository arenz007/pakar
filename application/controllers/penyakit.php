<?php defined('BASEPATH') OR die('No direct access allowed.');

class Penyakit extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('PenyakitModel', 'penyakit');
		$this->load->model('RelasiModel', 'relasi');
    }
    
	public function index() {
        $this->sesi->delete('edit');
		$this->sesi->delete('detail');
		
        $result = $this->penyakit->get();
        $data['result'] = $result;
        
        $this->layout['content'] = $this->load->view('penyakit/data', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
    
    public function tambah() {
        if ($this->input->post()) {
            if ($this->validasi()) {
                $this->penyakit->insert($this->input->post());
                $this->sesi->set('alert', 'Data telah disimpan');
                redirect('penyakit');
            }
        }
        
        $data['subtitle'] = 'Tambah Data';
        $data['action']   = site_url('penyakit/tambah');
        $this->layout['content'] = $this->load->view('penyakit/form', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    public function edit() {
        if ($this->input->post() AND $this->input->is_ajax_request()) {
            $this->sesi->set(array('edit' => array('kode' => $this->input->post('kode'))));
            exit;
        }
        
        if ($this->sesi->get('edit')) {
            $edit   = $this->sesi->get('edit');
            $result = $this->penyakit->get(array('kode_penyakit' => $edit['kode']));
            $data['result'] = $result->row();
        }
        else {
            redirect('penyakit');
        }
        
        if ($this->input->post()) {
            if ($this->validasi()) {
                $this->penyakit->update($this->input->post(), $edit['kode']);
                $this->sesi->set('alert', 'Data telah diedit');
                redirect('penyakit');
            }
        }
        
        $data['subtitle'] = 'Edit Data';
        $data['action']   = site_url('penyakit/edit');
        $this->layout['content'] = $this->load->view('penyakit/form', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    public function hapus() {
        if ($this->input->post() AND $this->input->is_ajax_request()) {
            $this->penyakit->delete($this->input->post('kode'));
            $this->sesi->set('alert', 'Data telah dihapus');
            exit;
        }
		else {
            redirect('penyakit');
        }
    }
	
	public function detail() {
        if ($this->input->post() AND $this->input->is_ajax_request()) {
            $this->sesi->set(array('detail' => array('kode' => $this->input->post('kode'))));
            exit;
        }
        
        if ($this->sesi->get('detail')) {
            $detail = $this->sesi->get('detail');
            $result = $this->relasi->get(array('kode_penyakit' => $detail['kode']));
            $data['result'] = $result->row();
        }
        else {
            redirect('penyakit');
        }
		
		$data['subtitle'] = 'Detail Penyakit';
        $data['action']   = site_url('penyakit/detail');
        $this->layout['content'] = $this->load->view('penyakit/detail', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    private function validasi() {
        $this->load->library('form_validation', NULL, 'validation');
        $this->validation->set_error_delimiters('<div class="error">', '</div>');
        
        $rules = array(
            array(
                'field' => 'textKode',
                'label' => 'Kode Penyakit',
                'rules' => 'required'
            ),
            array(
                'field' => 'textNama',
                'label' => 'Nama Penyakit',
                'rules' => 'required'
            )
        );
        
        $this->validation->set_rules($rules);
        return $this->validation->run();
    }
}