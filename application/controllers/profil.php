<?php defined('BASEPATH') OR die('No direct access allowed.');

class Profil extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel', 'user');
        $this->load->model('KonsultasiModel', 'konsultasi');
    }
    
	public function index() {
        $result = $this->user->get(array('username' => $this->sesiLogin['username']));
        $data['result'] = $result->row();
        
        $histori = $this->konsultasi->get($this->sesiLogin['username']);
        $data['histori'] = $histori;
        
        $this->layout['content'] = $this->load->view('profil/index', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
	
	public function histori() {
		if ($this->input->post() AND $this->input->is_ajax_request()) {
			$result = $this->konsultasi->getDetail($this->input->post('id'));
			
			$data = array(
				'histori' => $result['histori']->row(),
				'detail'  => $result['detail']
			);
			
			echo $this->load->view('profil/histori', $data, TRUE);
			exit;
		}
		else {
            redirect('profil');
        }	
	}
    
    public function ubah() {
        $result = $this->user->get(array('username' => $this->sesiLogin['username']));
        $data['result'] = $result->row();
        
        if ($this->input->post()) {
            $this->load->library('form_validation', NULL, 'validation');
            $this->validation->set_error_delimiters('<div class="error">', '</div>');
            
            $rules = array(
                array(
                    'field' => 'textNama',
                    'label' => 'Nama',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textAlamat',
                    'label' => 'Alamat',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'radioJenisKelamin',
                    'label' => 'Jenis Kelamin',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textUsia',
                    'label' => 'Usia',
                    'rules' => 'required|numeric'
                )
            );
            
            $this->validation->set_rules($rules);
            if ($this->validation->run()) {
                $this->user->update($this->input->post(), $this->sesiLogin['username']);
                $this->sesi->set('msg', 'Profil user telah diubah');
                redirect('profil/ubah');
            }
        }
        
        $this->layout['content'] = $this->load->view('profil/ubah', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    public function password() {
		if ($this->input->post()) {
            $this->load->library('form_validation', NULL, 'validation');
            $this->validation->set_error_delimiters('<div class="error">', '</div>');
            
            $rules = array(
                array(
                    'field' => 'textPasswordLama',
                    'label' => 'Password Lama',
                    'rules' => 'required|callback_cekPwdLama'
                ),
                array(
                    'field' => 'textPasswordBaru1',
                    'label' => 'Password',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textPasswordBaru2',
                    'label' => 'Password',
                    'rules' => 'required|matches[textPasswordBaru1]'
                )
            );
            
            $this->validation->set_rules($rules);
            if ($this->validation->run()) {
				$this->user->ubahPwd($this->input->post('textPasswordBaru1'));
                $this->sesi->set('msg', 'Password user telah diubah');
                redirect('profil/password');
            }
        }
        $this->layout['content'] = $this->load->view('profil/password', '', TRUE);
        $this->load->view('layout', $this->layout);
    }
	
	public function cekPwdLama($str) {
		if ($this->user->cekPwd($str))
			return TRUE;
		else {
			$this->validation->set_message('cekPwdLama', 'Password lama salah');
			return FALSE;
		}
	}
}