<?php defined('BASEPATH') OR die('No direct access allowed.');

class Pendaftaran extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel', 'user');
    }
    
	public function index() {
        if ($_POST) {
            $this->load->library('form_validation', NULL, 'validation');
            $this->validation->set_error_delimiters('<div class="error">', '</div>');
            
            $rules = array(
                array(
                    'field' => 'textUsername',
                    'label' => 'Username',
                    'rules' => 'required|is_unique[user.username]'
                ),
                array(
                    'field' => 'textPassword1',
                    'label' => 'Password',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textPassword2',
                    'label' => 'Password',
                    'rules' => 'required|matches[textPassword1]'
                ),
                array(
                    'field' => 'textNama',
                    'label' => 'Nama',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textAlamat',
                    'label' => 'Alamat',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'radioJenisKelamin',
                    'label' => 'Jenis Kelamin',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'textUsia',
                    'label' => 'Usia',
                    'rules' => 'required|numeric'
                )
            );
            
            $this->validation->set_rules($rules);
            if ($this->validation->run()) {
                $this->user->insert($this->input->post());
                $this->sesi->set('msg', 'Pendaftaran telah sukses, silahkan lanjutkan untuk Login');
                redirect('pendaftaran');
            }
        }
        
        $this->layout['content'] = $this->load->view('pendaftaran', '', TRUE);
        $this->load->view('layout', $this->layout);
	}
	
	
}