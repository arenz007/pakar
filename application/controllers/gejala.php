<?php defined('BASEPATH') OR die('No direct access allowed.');

class Gejala extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('GejalaModel', 'gejala');
    }
    
	public function index() {
        $this->sesi->delete('edit');
        $result = $this->gejala->get();
        $data['result'] = $result;
        
        $this->layout['content'] = $this->load->view('gejala/data', $data, TRUE);
        $this->load->view('layout', $this->layout);
	}
    
    public function tambah() {
        if ($_POST) {
            if ($this->validasi()) {
                $this->gejala->insert($this->input->post());
                $this->sesi->set('alert', 'Data telah disimpan');
                redirect('gejala');
            }
        }
        
        $data['subtitle'] = 'Tambah Data';
        $data['action']   = site_url('gejala/tambah');
        $this->layout['content'] = $this->load->view('gejala/form', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    public function edit() {
        if ($this->input->is_ajax_request()) {
            $this->sesi->set(array('edit' => array('kode' => $this->input->post('kode'))));
            exit;
        }
        
        if ($this->sesi->get('edit')) {
            $edit   = $this->sesi->get('edit');
            $result = $this->gejala->get(array('kode_gejala' => $edit['kode']));
            $data['result'] = $result->row();
        }
        else {
            redirect('gejala');
        }
        
        if ($_POST) {
            if ($this->validasi()) {
                $this->gejala->update($this->input->post(), $edit['kode']);
                $this->sesi->set('alert', 'Data telah diedit');
                redirect('gejala');
            }
        }
        
        $data['subtitle'] = 'Edit Data';
        $data['action']   = site_url('gejala/edit');
        $this->layout['content'] = $this->load->view('gejala/form', $data, TRUE);
        $this->load->view('layout', $this->layout);
    }
    
    public function hapus() {
        if ($this->input->is_ajax_request()) {
            $this->gejala->delete($this->input->post('kode'));
            $this->sesi->set('alert', 'Data telah dihapus');
            exit;
        }
    }
    
    private function validasi() {
        $this->load->library('form_validation', NULL, 'validation');
        $this->validation->set_error_delimiters('<div class="error">', '</div>');
        
        $rules = array(
            array(
                'field' => 'textKode',
                'label' => 'Kode Gejala',
                'rules' => 'required'
            ),
            array(
                'field' => 'textNama',
                'label' => 'Nama Gejala',
                'rules' => 'required'
            )
        );
        
        $this->validation->set_rules($rules);
        return $this->validation->run();
    }
}