function inputErrorMsg(textbox) {
    
    if (textbox.value == '') {
        textbox.setCustomValidity('Isikan');
    }
    else {
        textbox.setCustomValidity('');
    }
    
    return true;
}